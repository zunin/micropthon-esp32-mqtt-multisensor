WIFI_SSID = "WIFI_SSID"
WIFI_PASSWORD = "WIFI_PASSWORD-"

MQTT_USER = "MQTT_USER"
MQTT_PASS = "MQTT_PASS"
MQTT_SERVER = "homeassistant.local"
MQTT_PORT = 1883
MQTT_TOPIC = b"home/living_room/indoorsensorpack"

try:
    from local_settings import *
except ImportError:
    pass
