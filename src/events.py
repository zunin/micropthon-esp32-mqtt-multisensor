from ucollections import namedtuple

DHTEvent = namedtuple("DHTEvent", (
    "temperature",
    "humidity"
))


MQ9Event = namedtuple("MQ9Event", [
    "co_level"
])

LightEvent = namedtuple("LightEvent", (
    "light"
))
