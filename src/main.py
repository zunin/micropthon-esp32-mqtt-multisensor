import time
import ubinascii
import ujson as json
import network
from machine import unique_id
from umqtt.simple import MQTTClient
from settings import WIFI_SSID, WIFI_PASSWORD, MQTT_PASS, MQTT_USER, MQTT_SERVER, MQTT_PORT, MQTT_TOPIC
from sensors import DHT11Sensor, LightSensor, MQ9Sensor
from events import DHTEvent, LightEvent, MQ9Event

CLIENT_ID = ubinascii.hexlify(unique_id())
MQTT_CLIENT = MQTTClient(CLIENT_ID, MQTT_SERVER, MQTT_PORT, MQTT_USER, MQTT_PASS)

dht_sensor = DHT11Sensor(32)
light_sensor = LightSensor(34)
mq9_sensor = MQ9Sensor(35)

def do_sensor_loop():
    try:
        MQTT_CLIENT.connect()
        dhtEvent = dht_sensor.read()
        lightEvent = light_sensor.read()
        mq9Event = mq9_sensor.read()

        event = json.dumps({
            "humidity": dhtEvent.humidity,
            "temperature": dhtEvent.temperature,
            "co_level": mq9Event.co_level,
            "light": lightEvent.light
        })

        print(event)

        MQTT_CLIENT.publish(MQTT_TOPIC, event)
    except Exception as e:
        print(e)
    finally:
        MQTTClient.disconnect()


def main():
    while True:
        try:
            do_connect()
            do_sensor_loop()
        except Exception:
            pass
        finally:
            time.sleep(30)


def do_connect():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(WIFI_SSID, WIFI_PASSWORD)
        while not wlan.isconnected():
            pass
        print('network config:', wlan.ifconfig())


if __name__ == "__main__":
    main()
