from machine import Pin, ADC
from dht import DHT11
from events import DHTEvent, LightEvent, MQ9Event


class DHT11Sensor:
    sensor = None

    def __init__(self, pin_number):
        self.sensor = DHT11(Pin(pin_number))

    def read(self):
        self.sensor.measure()
        return DHTEvent(
            temperature=str(self.sensor.temperature()),
            humidity=str(self.sensor.humidity())
        )


class LightSensor:
    sensor = None

    def __init__(self, pin_number):
        self.sensor = ADC(Pin(pin_number))

    def read(self):
        return LightEvent(
            light=str(self.sensor.read())
        )


class MQ9Sensor:
    sensor = None

    def __init__(self, pin_number):
        self.sensor = ADC(Pin(pin_number))

    def read(self):
        return MQ9Event(
            co_level=str(self.sensor.read())
        )
