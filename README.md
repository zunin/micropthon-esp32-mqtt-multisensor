# Required software

This project uses [micropy-cli](https://pypi.org/project/micropy-cli/) for setup and [pymakr](https://marketplace.visualstudio.com/items?itemName=pycom.Pymakr) for developmet.

It was developed using [Micropython 1.12](https://micropython.org/download/esp32/).
